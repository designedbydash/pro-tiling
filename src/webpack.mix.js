const mix = require('laravel-mix');
mix.setPublicPath('../web')
.js('js/index.js','js/').sourceMaps()
.sass('scss/app.scss','css/');